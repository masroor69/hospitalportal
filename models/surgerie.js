var mongoose = require('mongoose');
const moment = require('moment');

var Patient= require('../models/patient');

var Schema = mongoose.Schema;
var Doctor = new Schema({ 
                    _id: Schema.Types.ObjectId,
                    title : {type: String,},  
                    name: {type: String, },
                });
var Patient = new Schema({
                    _id: Schema.Types.ObjectId,
                    title : {type: String,},  
                    name: {type: String,},
                });
var SurgerieSchema = new Schema({
    title : {type: String,  required: [true, 'Title is required !']},  
    active: String,
    startDate: Date,
    endDate: Date,
    patient:Patient,
    doctor:[Doctor],
    room: {type:String, enum:["Room 1","Room 2" , "Room 3" , "Room 4"] }
    });

SurgerieSchema
.virtual('url')
.get(function () {
  return '/surgerie/'+this._id;
});

SurgerieSchema
.virtual('startDateFormated')
.get(function () {
  return moment(this.startDate).format('YYYY/MM/DD, HH:mm');
});

SurgerieSchema
.virtual('endDateFormated')
.get(function () {
  return moment(this.endDate).format('YYYY/MM/DD, HH:mm');
});
module.exports = mongoose.model('Surgerie', SurgerieSchema);
