var mongoose = require('mongoose');


var Schema = mongoose.Schema;

var DoctorsSchema = new Schema(
    {
    title : {type: String,  required: [true, 'Title is required !']  , enum: ['Mr', 'Mrs', 'Miss',]},  
    name: {type: String, required: [true, 'Name is required !'], max: 100},
    age: {type: Number, required: [true, 'Age is required !']},
    }
  );

DoctorsSchema
.virtual('fullName')
.get(function () {
  return this.title +', '+this.name;
});
DoctorsSchema
.virtual('url')
.get(function () {
  return '/doctor/'+this._id;
});



module.exports = mongoose.model('Doctor', DoctorsSchema);
