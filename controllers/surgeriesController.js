var Doctor = require('../models/doctor');
var Patient = require('../models/patient');
var Surgerie = require('../models/surgerie');


const { check,body,validationResult } = require('express-validator/check');
const { sanitizeBody } = require('express-validator/filter');
const moment = require('moment');
var async = require('async');


exports.index = function(req, res) { 
    res.send('The Index Method');
};

exports.surgeries_list = function (req, res) {
    res.locals.createPagination = function (pages, page) {
        var url = require('url')
          , qs = require('querystring')
          , params = qs.parse(url.parse(req.url).query)
          , str = '' 
        params.page = 0
        var clas = page == 0 ? "active" : "no"
        str += '<li class="'+clas+'"><a href="?'+qs.stringify(params)+'">First</a></li>'
        for (var p = 1; p < pages; p++) {
          params.page = p
          clas = page == p ? "active" : "no"
          str += '<li class="'+clas+'"><a href="?'+qs.stringify(params)+'">'+ p +'</a></li>'
        }
        params.page = --p
        clas = page == params.page ? "active" : "no"
        str += '<li class="'+clas+'"><a href="?'+qs.stringify(params)+'">Last</a></li>'
      
        return str
      }
    var perPage = 7
      , page = req.param('page') > 0 ? req.param('page') : 0
      async.parallel({
        doctors: function(callback){
            Doctor.find({})
            .exec(callback)
           }
        },function (err, results) {
                if (err) { return next(err) }
                    Surgerie
                    .find()
                    .limit(perPage)
                    .skip(perPage * page)
                    .sort({name: 'asc'})
                    .exec(function (err, surgeries) {
                    Surgerie.count().exec(function (err, count) {
                        res.render('./surgeries/list', {
                            surgeries: surgeries
                            , page: page
                            , pages: count / perPage,
                            doctorsList : results.doctors
                        })
                    })
                    })
                })
};

exports.surgeries_list_post = function (req, res) {
    res.locals.createPagination = function (pages, page) {
        var url = require('url')
          , qs = require('querystring')
          , params = qs.parse(url.parse(req.url).query)
          , str = '' 
        params.page = 0
        var clas = page == 0 ? "active" : "no"
        str += '<li class="'+clas+'"><a href="?'+qs.stringify(params)+'">First</a></li>'
        for (var p = 1; p < pages; p++) {
          params.page = p
          clas = page == p ? "active" : "no"
          str += '<li class="'+clas+'"><a href="?'+qs.stringify(params)+'">'+ p +'</a></li>'
        }
        params.page = --p
        clas = page == params.page ? "active" : "no"
        str += '<li class="'+clas+'"><a href="?'+qs.stringify(params)+'">Last</a></li>'
      
        return str
      }
    
    var perPage = 7
      , page = req.param('page') > 0 ? req.param('page') : 0
      async.parallel({
        doctors: function(callback){
            Doctor.find({})
            .exec(callback)
           }
        },function (err, results) {
                if (err) { return next(err) }
                    var queryString = ""; 
                    let query = Surgerie
                    .find();
                    if(req.body.active){
                        query.where({'active': 'on' });
                        queryString += '- Active  '
                    }
                    if(req.body.deactive){
                        query.where({'active': {$eq:null} });
                        queryString += '- DeActive  '
                        
                    }
                    if(req.body.doctors){
                        var DoctorObj = JSON.parse(req.body.doctors)
                        query.where({'doctor._id': {$in:DoctorObj._id} });
                        queryString +='- Doctor =  ' + DoctorObj.title +' '+  DoctorObj.name
                        
                    }
                    if(req.body.startTime){
                        query.where({'startDate': {"$gte": req.body.startTime} });
                        queryString += '- From >=   ' + moment(req.body.startTime).format('YYYY/MM/DD, HH:mm');
                    }
                    if(req.body.endTime){
                        query.where({'endDate': {"$lt": req.body.endTime} });
                        queryString += ' - To <   ' + moment(req.body.endTime).format('YYYY/MM/DD, HH:mm');
                    }
                    query.limit(perPage);
                    query.skip(perPage * page);
                    query.sort({name: 'asc'});
                    query.exec(function (err, surgeries) {
                    Surgerie.count().exec(function (err, count) {
                        res.render('./surgeries/list', {
                            surgeries: surgeries
                            , page: page
                            , pages: count / perPage,
                            doctorsList : results.doctors , queryString : queryString
                        })
                    })
                    })
                })
};

exports.surgeries_detail = function(req, res, next) {
    res.send('NOT IMPLEMENTED:  DETAIL');
};

exports.surgeries_create_get = function(req, res) {
    async.parallel({
        patients: function(callback){
              Patient.find({})
              .exec(callback);
           },
        doctors: function(callback){
            Doctor.find({})
            .exec(callback)
           }
        },function (err, results) {
                if (err) { return next(err) }
                    res.render('./surgeries/surgeries_form', { title: 'Create Surgerie' ,doctors:  results.doctors ,patients: results.patients});
                })
            };
            
exports.surgeries_create_post =[
    body('title', 'Doctor e Invalid between 0 - 99 ').trim(),
    (req, res, next) => {
            let doctorsObj = new Array();
            if(Array.isArray(req.body.doctors)){
            req.body.doctors.map((doctor)=>{
                doctorsObj.push(JSON.parse( {
                      _id,
                      title,
                      name,  
                  }=  doctor ));  
           });
        }else{
            doctorsObj.push( JSON.parse({
                _id,
                title,
                name,  
            }=  req.body.doctors ));
        }
        var endDateFormated =   moment(req.body.endTime).format('YYYY/MM/DD, HH:mm');
        var startDateFormated =  moment(req.body.startTime).format('YYYY/MM/DD, HH:mm');
        const errors = validationResult(req);
        doctorsObj.forEach((doctor)=>{
             Surgerie.find()
            .where({'doctor._id': {$in:doctor._id} })
            .where({'startDate': {"$gte": req.body.startTime} })
            .where({'endDate': {"$lte": req.body.endTime} })
            .exec(function (err, found_surgeries) {
                if(found_surgeries.length>0){
                    async.parallel({
                        patients: function(callback){
                              Patient.find({})
                              .exec(callback);
                           },
                        doctors: function(callback){
                            Doctor.find({})
                            .exec(callback)
                           }
                        },function (err, results) {
                                if (err) { return next(err) }
                                    res.render('./surgeries/surgeries_form', { title: 'Create Surgerie' ,doctors:  results.doctors ,patients: results.patients ,errors : {msg:`Dr ${doctor.title} ${doctor.name} is Busy That Day ... `} });
                                })        
            }else{
            var surgerie = new Surgerie({
                        title : req.body.title,
                        active: req.body.active ,
                        startDate:startDateFormated,
                        endDate:endDateFormated,
                        patient:JSON.parse(req.body.patient),
                        doctor: [...doctorsObj],
                        room: req.body.room,       
                }
            );
                surgerie.save(function (err) {
                    if (err) {  next(err); }
                    res.redirect('./surgeries');
                });
            }
            });
        });
    }
]

exports.surgeries_delete_get =function(req, res, next) {
    Surgerie.findOneAndRemove(req.params.id,function(err) {
    if (err) { return next(err); }
    res.redirect('/../../surgerie/surgeries');
    });
};

exports.surgeries_delete_post = function(req, res) {
    res.send('NOT IMPLEMENTED:  Delete POST');
};

exports.surgeries_update_get = function(req, res) {
    res.send('NOT IMPLEMENTED:  update POST');
};
exports.surgeries_update_post = function(req, res) {
    res.send('NOT IMPLEMENTED: Update POST');
};
