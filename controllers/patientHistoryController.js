var Patient = require('../models/patient');
var Disease = require('../models/disease');
var PatientDisease = require('../models/PaientsDisease');

const { body,validationResult } = require('express-validator/check');
const { sanitizeBody } = require('express-validator/filter');

var async = require('async');

exports.history_create_post = (req, res, next) => {
        let patientId = req.params.pationt_id;
        let batchInsert = Array.isArray(req.body.disasesList); 
        let docArr = new Array();
        if (batchInsert) {
            req.body.disasesList.forEach(disaseId => {
                var history  = new PatientDisease({
                    patient: patientId,
                    disease:disaseId
                });
                docArr.push(history);
            });
            PatientDisease.insertMany(docArr,(err,result) =>{
                    if(err){next(err)}
                    res.redirect('/patient/update/'+patientId);
            });
        }
        else{
            PatientDisease.find({})
                .where('disease').equals(req.body.disasesList)
                .where('patient').equals(patientId)
                .exec((err,result)=>{
                    if(err){ next(err)}
                    
                    var  history  = new PatientDisease(
                        {
                        patient: patientId,
                        disease:req.body.disasesList
                    });
                    history.save(function (err) {
                        if (err) { return next(err); }
                        res.redirect('/patient/update/'+patientId);
                      });
                });        
        }
    };

exports.history_delete_get = function(req, res) {
    let patientId = req.params.pationt_id;
    let diseaseId = req.params.disease_id;
    PatientDisease.findOneAndRemove(diseaseId,function(err) {
        if (err) { return next(err); }
        res.redirect('/patient/update/'+patientId);
        });
};