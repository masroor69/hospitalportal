var Patient = require('../models/patient');
var Disease = require('../models/disease');
var PatientDisease = require('../models/PaientsDisease');

const { body,validationResult } = require('express-validator/check');
const { sanitizeBody } = require('express-validator/filter');

var async = require('async');

exports.index = function(req, res) { 
    res.render('./patient/index');
};
exports.patient_list = function (req, res) {
    res.locals.createPagination = function (pages, page) {
        var url = require('url')
          , qs = require('querystring')
          , params = qs.parse(url.parse(req.url).query)
          , str = '' 
        params.page = 0
        var clas = page == 0 ? "active" : "no"
        str += '<li class="'+clas+'"><a href="?'+qs.stringify(params)+'">First</a></li>'
        for (var p = 1; p < pages; p++) {
          params.page = p
          clas = page == p ? "active" : "no"
          str += '<li class="'+clas+'"><a href="?'+qs.stringify(params)+'">'+ p +'</a></li>'
        }
        params.page = --p
        clas = page == params.page ? "active" : "no"
        str += '<li class="'+clas+'"><a href="?'+qs.stringify(params)+'">Last</a></li>'
      
        return str
      }
    var perPage = 10
      , page = req.param('page') > 0 ? req.param('page') : 0
  
      Patient
      .find()
      .limit(perPage)
      .skip(perPage * page)
      .sort({name: 'asc'})
      .exec(function (err, patient) {
        Patient.count().exec(function (err, count) {
            res.render('./patient/list', {
                        patients: patient
                        , page: page
                        , pages: count / perPage
          })
        })
      })
};


exports.patient_detail = function(req, res) {
    async.parallel({
        historys : function(callback){
              PatientDisease.find({'patient':req.params.id}).populate('disease')
              .exec(callback);
           },
        patient: function(callback){
            Patient.findById(req.params.id)
            .exec(callback)
           }
        },function (err, results) {
                if (err) { return next(err) }
                if (results==null) { 
                    var err = new Error('Patient not found');
                    err.status = 404;
                    } 
                res.render('./patient/patient_detail', { title: 'Disease:', patient:  results.patient,historys: results.historys});
                })
};

exports.patient_create_get = function(req, res) {
    res.render('./patient/patient_form', { title: 'Create Patient' });
};

exports.patient_create_post =[
    body('title', 'Patient title required').isLength({ min: 1 }).trim(),
    body('name', 'Patient Name required').isLength({ min: 1 }).trim(),
    body('age', 'Patient Age Invalid between 0 - 99 ').isLength({ min: 1 , max:2 }).trim(),
    sanitizeBody('name').trim().escape(),
    sanitizeBody('age').trim().escape(),
    (req, res, next) => {
        const errors = validationResult(req);
        var patient = new Patient(
          { title,
            name,
            age,
            smoking
          }= req.body
        );
        if (!errors.isEmpty()) {
            res.render('./patient/patient_form', { title: 'Create Patient', patient: patient, errors: errors.array()});
        return;
        }
        else {
            patient.save(function (err) {
                if (err) { return next(err); }
                res.redirect(patient.url);
              });
        }
    }
 ]

exports.patient_delete_get = function(req, res) {
    Patient.findOneAndRemove(req.body.id,function(err) {
        if (err) { return next(err); }
        res.redirect('/patient/patients');
        });
};

exports.patient_delete_post = function(req, res) {
    res.send('NOT IMPLEMENTED: Patient delete POST');
};

exports.patient_update_get = function(req, res) {
    async.parallel({
      historys : function(callback){
            PatientDisease.find({'patient':req.params.id}).populate('disease')
            .exec(callback);
         }, 
    
        disease : function(callback){
            Disease.find({})
            .exec(callback);
         },
        patient : function(callback){
            Patient.findById(req.params.id )
            .exec(callback);
         }
    },
     function (err, results) {
        if(err){return next(err);}
        res.render('./patient/patient_form', { title: 'Update Patient' ,update :'true', patient: results.patient, diseases: results.disease, historys: results.historys,});
    });
};

exports.patient_update_post  =[
     
        body('title', 'Patient title required').isLength({ min: 1 }).trim(),
        body('name', 'Patient Name required').isLength({ min: 1 }).trim(),
        body('age', 'Patient Age Invalid between 0 - 99 ').isLength({ min: 1 , max:2 }).trim(),
        sanitizeBody('name').trim().escape(),
        sanitizeBody('age').trim().escape(),
    (req, res, next) => {
        const errors = validationResult(req);
        var patient = new Patient(
           {title: req.body.title,
            name: req.body.name,
            age: req.body.age,
            smoking: req.body.smoking,
            _id: req.params.id
        }
        );
        if (!errors.isEmpty()) {
            res.render('./patient/patient_form', { title: 'Update patient' ,update :'true', patient: patient, errors: errors.array()});
        return;
        }
        else {
            Patient.findByIdAndUpdate(req.params.id, patient, {}, function (err,thepatient) {
                    if (err) { next(err) }
                    res.redirect(thepatient.url);
                });                        
            }
    }
 ]