var Docotor = require('../models/doctor');
var Proficiency = require('../models/proficiency')
var DoctorsProficiency = require('../models/doctorsProficiency');

const { body,validationResult } = require('express-validator/check');
const { sanitizeBody } = require('express-validator/filter');

var async = require('async');

exports.experience_create_post = (req, res, next) => {
        let doctorId = req.params.doctor_id;
        let batchInsert = Array.isArray(req.body.proficiencyList); 
        let docArr = new Array();
        if (batchInsert) {
            req.body.proficiencyList.forEach(proficiencyId => {
                var experience  = new DoctorsProficiency({
                    doctor: doctorId,
                    proficiency:proficiencyId
                });
                docArr.push(experience);
            });
            DoctorsProficiency.insertMany(docArr,(err,result) =>{
                    if(err){next(err)}
                    res.redirect('/doctor/update/'+doctorId);
            });
        }
        else{
            DoctorsProficiency.find({})
                .where('doctor').equals(req.body.proficiencyList)
                .where('patient').equals(doctorId)
                .exec((err,result)=>{
                    if(err){ next(err)}
                    
                    var  experience  = new DoctorsProficiency(
                        {
                            doctor: doctorId,
                            proficiency:req.body.proficiencyList
                    });
                    experience.save(function (err) {
                        if (err) { return next(err); }
                        res.redirect('/doctor/update/'+doctorId);
                      });
                });        
        }
    };

exports.experience_delete_get = function(req, res) {
    let doctorId = req.params.doctor_id;
    let experienceId = req.params.experience_id;
    DoctorsProficiency.findOneAndRemove(experienceId,function(err) {
        if (err) { return next(err); }
        res.redirect('/doctor/update/'+doctorId);
        });
};