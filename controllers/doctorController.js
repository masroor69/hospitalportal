var Doctor = require('../models/doctor');
var Proficiency = require('../models/proficiency');
var DoctorsProficiency = require('../models/doctorsProficiency')

const { body,validationResult } = require('express-validator/check');
const { sanitizeBody } = require('express-validator/filter');

var async = require('async');

exports.index = function(req, res) { 
    res.render('./doctor/index');
};

exports.doctor_list = function (req, res) {
    res.locals.createPagination = function (pages, page) {
        var url = require('url')
          , qs = require('querystring')
          , params = qs.parse(url.parse(req.url).query)
          , str = '' 
        params.page = 0
        var clas = page == 0 ? "active" : "no"
        str += '<li class="'+clas+'"><a href="?'+qs.stringify(params)+'">First</a></li>'
        for (var p = 1; p < pages; p++) {
          params.page = p
          clas = page == p ? "active" : "no"
          str += '<li class="'+clas+'"><a href="?'+qs.stringify(params)+'">'+ p +'</a></li>'
        }
        params.page = --p
        clas = page == params.page ? "active" : "no"
        str += '<li class="'+clas+'"><a href="?'+qs.stringify(params)+'">Last</a></li>'
      
        return str
      }
    var perPage = 10
      , page = req.param('page') > 0 ? req.param('page') : 0
  
      Doctor
      .find()
      .limit(perPage)
      .skip(perPage * page)
      .sort({name: 'asc'})
      .exec(function (err, doctor) {
        Doctor.count().exec(function (err, count) {
            res.render('./doctor/list', {
                        doctors: doctor
                        , page: page
                        , pages: count / perPage
          })
        })
      })
};

exports.doctor_detail = function(req, res, next) {
    async.parallel({
        experiences : function(callback){
              DoctorsProficiency.find({'doctor':req.params.id}).populate('proficiency')
              .exec(callback);
           },
        doctor: function(callback){
            Doctor.findById(req.params.id)
            .exec(callback)
           }
        },function (err, results) {
                if (err) { return next(err) }
                if (results==null) { 
                    var err = new Error('Doctor not found');
                    err.status = 404;
                    } 
                    res.render('./doctor/doctor_detail', { title: 'Doctor:', doctor:  results.doctor ,experiences: results.experiences});
                })
};

exports.doctor_create_get = function(req, res) {
    res.render('./doctor/doctor_form', { title: 'Create Doctor' });
};

exports.doctor_create_post =[
        body('title', 'Doctor title required').isLength({ min: 1 }).trim(),
        body('name', 'Doctor Name required').isLength({ min: 1 }).trim(),
        body('age', 'Doctor Age Invalid between 0 - 99 ').isLength({ min: 1 , max:2 }).trim(),
        sanitizeBody('name').trim().escape(),
        sanitizeBody('age').trim().escape(),
        (req, res, next) => {
        const errors = validationResult(req);
        var doctor = new Doctor(
            {title,
            name,
            age
        }=req.body
        );
        if (!errors.isEmpty()) {
            res.render('./doctor/doctor_form', { title: 'Create Doctor', doctor: doctor, errors: errors.array()});
        return;
        }
        else {
            doctor.save(function (err) {
                if (err) { return next(err); }
                res.redirect(doctor.url);
            });
        }
    }
 ]

exports.doctor_delete_get =function(req, res, next) {
   Doctor.findOneAndRemove(req.body.id,function(err) {
    if (err) { return next(err); }
    res.redirect('/doctor/doctors');
    });
};

exports.doctor_delete_post = function(req, res) {
    res.send('NOT IMPLEMENTED: Doctor Delete POST');
};

exports.doctor_update_get = function(req, res) {
    async.parallel({
        experiences : function(callback){
            DoctorsProficiency.find({'doctor':req.params.id}).populate('proficiency')
            .exec(callback);
         },
         proficiencys : function(callback){
            Proficiency.find({})
            .exec(callback);
         },
        doctor: function(callback){
            Doctor.findById(req.params.id)
            .exec(callback)
        },
      },
       function (err, results) {
          if(err){return next(err);}
          res.render('./doctor/doctor_form', { title: 'Update Doctor' ,update :'true', doctor: results.doctor, proficiencys: results.proficiencys, experiences: results.experiences,});
      });
};

exports.doctor_update_post =[
    body('title', 'Doctor title required').isLength({ min: 1 }).trim(),
        body('name', 'Doctor Name required').isLength({ min: 1 }).trim(),
        body('age', 'Doctor Age Invalid between 0 - 99 ').isLength({ min: 1 , max:2 }).trim(),
        sanitizeBody('name').trim().escape(),
        sanitizeBody('age').trim().escape(),
        (req, res, next) => {
        const errors = validationResult(req);
        var doctor = new Doctor(
            {title: req.body.title,
            name: req.body.name,
            age: req.body.age,
            _id : req.params.id
        }
        );
        if (!errors.isEmpty()) {
            res.render('./doctor/doctor_form', { title: 'Update Doctor' ,update :'true', doctor: doctor, errors: errors.array()});
        return;
        }
        else {
                Doctor.findByIdAndUpdate(req.params.id, doctor, {}, function (err,thedoctor) {
                    if (err) { next(err) }
                    res.redirect(thedoctor.url);
                });                        
            }
    }
 ]