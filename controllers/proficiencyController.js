var Proficiency = require('../models/proficiency');

const { body,validationResult } = require('express-validator/check');
const { sanitizeBody } = require('express-validator/filter');

exports.index = function(req, res) { 
    res.render('./proficiency/index');
};

exports.proficiency_list = function (req, res) {
    res.locals.createPagination = function (pages, page) {
        var url = require('url')
          , qs = require('querystring')
          , params = qs.parse(url.parse(req.url).query)
          , str = '' 
        params.page = 0
        var clas = page == 0 ? "active" : "no"
        str += '<li class="'+clas+'"><a href="?'+qs.stringify(params)+'">First</a></li>'
        for (var p = 1; p < pages; p++) {
          params.page = p
          clas = page == p ? "active" : "no"
          str += '<li class="'+clas+'"><a href="?'+qs.stringify(params)+'">'+ p +'</a></li>'
        }
        params.page = --p
        clas = page == params.page ? "active" : "no"
        str += '<li class="'+clas+'"><a href="?'+qs.stringify(params)+'">Last</a></li>'
      
        return str
      }
    var perPage = 10
      , page = req.param('page') > 0 ? req.param('page') : 0
  
      Proficiency
      .find()
      .limit(perPage)
      .skip(perPage * page)
      .sort({name: 'asc'})
      .exec(function (err, proficiency) {
        Proficiency.count().exec(function (err, count) {
            res.render('./proficiency/list', {
                        proficiencys: proficiency
                        , page: page
                        , pages: count / perPage
          })
        })
      })
};

exports.proficiency_detail = function(req, res, next) {
    
    Proficiency.findById(req.params.id)
    .exec(function (err, proficiency) {
      if (err) { return next(err) }
      if (proficiency==null) { 
          var err = new Error('Proficiency not found');
          err.status = 404;
        }
      res.render('./proficiency/proficiency_detail', { title: 'Proficiency:', proficiency:  proficiency});
    })
};

exports.proficiency_create_get = function(req, res) {
    res.render('./proficiency/proficiency_form', { title: 'Create Proficiency' });
};

exports.proficiency_create_post =[
        body('title', 'Proficiency title required').isLength({ min: 1 }).trim(),
        sanitizeBody('title').trim().escape(),
        (req, res, next) => {
        const errors = validationResult(req);
        var proficiency = new Proficiency(
            {title }=req.body
        );
        if (!errors.isEmpty()) {
            res.render('./proficiency/proficiency_form', { title: 'Create Proficiency', proficiency: proficiency, errors: errors.array()});
        return;
        }
        else {
            proficiency.save(function (err) {
                if (err) { return next(err); }
                res.redirect(proficiency.url);
            });
        }
    }
 ]

exports.proficiency_delete_get =function(req, res, next) {
   Proficiency.findOneAndRemove(req.body.id,function(err) {
    if (err) { return next(err); }
    res.redirect('/proficiency/proficiencys');
    });
};

exports.proficiency_delete_post = function(req, res) {
    res.send('NOT IMPLEMENTED: Proficiency Delete POST');
};

exports.proficiency_update_get = function(req, res) {
    Proficiency.findById(req.params.id )
          .exec( function(err, found_proficiency) {
                if (err) { return next(err); }
                if (found_proficiency) {
                    res.render('./proficiency/proficiency_form', { title: 'Update Proficiency' ,update :'true', proficiency: found_proficiency,});
                }
            });
};

exports.proficiency_update_post =[
    body('title', 'Proficiency title required').isLength({ min: 1 }).trim(),
    sanitizeBody('title').trim().escape(),
        (req, res, next) => {
        const errors = validationResult(req);
        var proficiency = new Proficiency(
            {title: req.body.title,
            _id : req.params.id
        }
        );
        if (!errors.isEmpty()) {
            res.render('./proficiency/proficiency_form', { title: 'Update Proficiency' ,update :'true', proficiency: proficiency, errors: errors.array()});
        return;
        }
        else {
                Proficiency.findByIdAndUpdate(req.params.id, proficiency, {}, function (err,theproficiency) {
                    if (err) { next(err) }
                    res.redirect(theproficiency.url);
                });                        
            }
    }
 ]