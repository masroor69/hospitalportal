var Disease = require('../models/disease');

const { body,validationResult } = require('express-validator/check');
const { sanitizeBody } = require('express-validator/filter');

exports.index = function(req, res) { 
    res.render('./disease/index');
};

exports.disease_list = function (req, res) {
    res.locals.createPagination = function (pages, page) {
        var url = require('url')
          , qs = require('querystring')
          , params = qs.parse(url.parse(req.url).query)
          , str = '' 
        params.page = 0
        var clas = page == 0 ? "active" : "no"
        str += '<li class="'+clas+'"><a href="?'+qs.stringify(params)+'">First</a></li>'
        for (var p = 1; p < pages; p++) {
          params.page = p
          clas = page == p ? "active" : "no"
          str += '<li class="'+clas+'"><a href="?'+qs.stringify(params)+'">'+ p +'</a></li>'
        }
        params.page = --p
        clas = page == params.page ? "active" : "no"
        str += '<li class="'+clas+'"><a href="?'+qs.stringify(params)+'">Last</a></li>'
      
        return str
      }
    var perPage = 10
      , page = req.param('page') > 0 ? req.param('page') : 0
  
      Disease
      .find()
      .select('title')
      .limit(perPage)
      .skip(perPage * page)
      .sort({name: 'asc'})
      .exec(function (err, disease) {
        Disease.count().exec(function (err, count) {
            res.render('./disease/list', {
                        disease: disease
                        , page: page
                        , pages: count / perPage
          })
        })
      })
};

exports.disease_detail = function(req, res, next) {
    
    Disease.findById(req.params.id)
    .exec(function (err, disease) {
      if (err) { return next(err) }
      if (disease==null) { 
          var err = new Error('Disease not found');
          err.status = 404;
        }
      res.render('./disease/disease_detail', { title: 'Disease:', disease:  disease});
    })
};

exports.disease_create_get = function(req, res) {
    res.render('./disease/disease_form', { title: 'Create disease' });
};

exports.disease_create_post =[
    body('title', 'Disease title required').isLength({ min: 1 }).trim(),
    sanitizeBody('title').trim().escape(),
    sanitizeBody('disc').trim().escape(),
    (req, res, next) => {
        const errors = validationResult(req);
        var disease = new Disease(
          { title,
          disc }= req.body
        );
        if (!errors.isEmpty()) {
            res.render('./disease/disease_form', { title: 'Create Disease', disease: disease, errors: errors.array()});
        return;
        }
        else {
            Disease.findOne({ 'title': req.body.title })
                .exec( function(err, found_disease) {
                     if (err) { return next(err); }
                     if (found_disease) {
                         res.redirect(found_disease.url);
                     }
                     else {
                        disease.save(function (err) {
                           if (err) { return next(err); }
                           res.redirect(disease.url);
                         });
                     }
                 });
        }
    }
 ]

exports.disease_delete_get =function(req, res, next) {
   Disease.findOneAndRemove(req.body.id,function(err) {
    if (err) { return next(err); }
    res.redirect('/disease/diseases');
    });
};

exports.disease_delete_post = function(req, res) {
    res.send('NOT IMPLEMENTED: Disease delete POST');
};

exports.disease_update_get = function(req, res) {
    Disease.findById(req.params.id )
                .exec( function(err, found_disease) {
                     if (err) { return next(err); }
                     if (found_disease) {
                         res.render('./disease/disease_form', { title: 'Update disease' ,update :'true', disease: found_disease,});
                     }
                    });
};

exports.disease_update_post =[
    body('title', 'Disease title required').isLength({ min: 1 }).trim(),
    sanitizeBody('title').trim().escape(),
    sanitizeBody('disc').trim().escape(),
    (req, res, next) => {
        const errors = validationResult(req);
        var disease = new Disease(
          { title: req.body.title,
            disc: req.body.disc,
            _id: req.params.id
        }
        );
        if (!errors.isEmpty()) {
            res.render('./disease/disease_form', { title: 'Update disease' ,update :'true', disease: disease, errors: errors.array()});
        return;
        }
        else {
                Disease.findByIdAndUpdate(req.params.id, disease, {}, function (err,thedisease) {
                    if (err) { next(err) }
                    res.redirect(thedisease.url);
                });                        
            }
    }
 ]