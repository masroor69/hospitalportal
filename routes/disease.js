var express = require('express');
var router = express.Router();


var disease_Controller = require('../controllers/diseaseController');


router.get('/', disease_Controller.index);

router.get('/create', disease_Controller.disease_create_get);

router.post('/create', disease_Controller.disease_create_post);

router.get('/diseases', disease_Controller.disease_list);

router.get('/delete/:id', disease_Controller.disease_delete_get);

router.get('/update/:id', disease_Controller.disease_update_get);

router.post('/update/:id', disease_Controller.disease_update_post);

router.get('/:id', disease_Controller.disease_detail);

module.exports = router;