var express = require('express');
var router = express.Router();


var surgeries_Controller = require('../controllers/surgeriesController');


router.get('/', surgeries_Controller.index);

router.get('/create', surgeries_Controller.surgeries_create_get);

router.post('/create', surgeries_Controller.surgeries_create_post);

router.get('/surgeries', surgeries_Controller.surgeries_list);

router.post('/surgeries', surgeries_Controller.surgeries_list_post);

router.get('/delete/:id', surgeries_Controller.surgeries_delete_get);

router.get('/update/:id', surgeries_Controller.surgeries_update_get);

router.post('/update/:id', surgeries_Controller.surgeries_update_post);

router.get('/:id', surgeries_Controller.surgeries_detail);

module.exports = router;