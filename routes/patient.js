var express = require('express');
var router = express.Router();


var patient_Controller = require('../controllers/patientController');


router.get('/', patient_Controller.index);

router.get('/create', patient_Controller.patient_create_get);

router.post('/create', patient_Controller.patient_create_post);

router.get('/patients', patient_Controller.patient_list);

router.get('/delete/:id', patient_Controller.patient_delete_get);

router.get('/update/:id', patient_Controller.patient_update_get);

router.post('/update/:id', patient_Controller.patient_update_post);

router.get('/:id', patient_Controller.patient_detail);

module.exports = router;