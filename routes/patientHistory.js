var express = require('express');
var router = express.Router();


var patient_history_Controller = require('../controllers/patientHistoryController');

router.post('/create/:pationt_id', patient_history_Controller.history_create_post);

router.get('/delete/:disease_id/:pationt_id', patient_history_Controller.history_delete_get);


module.exports = router;