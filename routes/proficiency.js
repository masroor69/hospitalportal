var express = require('express');
var router = express.Router();


var proficiency_Controller = require('../controllers/proficiencyController');


router.get('/', proficiency_Controller.index);

router.get('/create', proficiency_Controller.proficiency_create_get);

router.post('/create', proficiency_Controller.proficiency_create_post);

router.get('/proficiencys', proficiency_Controller.proficiency_list);

router.get('/delete/:id', proficiency_Controller.proficiency_delete_get);

router.get('/update/:id', proficiency_Controller.proficiency_update_get);

router.post('/update/:id', proficiency_Controller.proficiency_update_post);

router.get('/:id', proficiency_Controller.proficiency_detail);

module.exports = router;