var express = require('express');
var router = express.Router();


var doctor_Proficiency_Controller = require('../controllers/doctorProficiencyController');

router.post('/create/:doctor_id', doctor_Proficiency_Controller.experience_create_post);

router.get('/delete/:experience_id/:doctor_id', doctor_Proficiency_Controller.experience_delete_get);


module.exports = router;